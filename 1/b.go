package main

import (
	"container/ring"
	"fmt"
	"log"
	"os"
	"bufio"
	"strconv"
)

// Read the input file and return a slice of its lines
func readLines(path string) ([]int, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		i, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		lines = append(lines, i)
	}
	return lines, scanner.Err()
}

func main() {
	input, err := readLines("input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	// Load input into a ring buffer
	r := ring.New(len(input))
	for i := 0; i < r.Len(); i++ {
		r.Value = input[i]
		r = r.Next()
	}

	frequencySeen := make(map[int]bool)
	total := 0
	for {
		total += r.Value.(int)
		if frequencySeen[total] {
			fmt.Println(total)
			break
		} else {
			frequencySeen[total] = true
		}
		r = r.Next()
	}
}
