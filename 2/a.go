package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// Read the input file and return a slice of its lines
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func main() {
	input, err := readLines("input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
	twos, threes := 0, 0
	for _, str := range input {
		two, three := examine(str)
		if (two) {
			twos++
		}
		if (three) {
			threes++
		}
	}
	fmt.Println(twos * threes)
}

func examine(str string)(twos bool, threes bool) {
	frequencySeen := make(map[rune]int)
	for _, ch := range str {
		frequencySeen[ch]++
	}
	for _, v := range frequencySeen {
		if v == 2 {
			twos = true
		} else if v == 3 {
			threes = true
		}
	}
	return twos, threes
}
