package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// Read the input file and return a slice of its lines
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func main() {
	input, err := readLines("input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	for _, a := range input {
		// Examine each string against each of the others
		for _, b := range input {
			if differences(a, b) == 1 {
				fmt.Println(removeCommon(a, b))
				os.Exit(0)
			}
		}
	}
}

func differences(a string, b string)(diffs int) {
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			diffs++
		}
	}
	return
}

func removeCommon(a string, b string)(diffs string) {
	for i := 0; i < len(a); i++ {
		if a[i] == b[i] {
			diffs += string(a[i])
		}
	}
	return
}
