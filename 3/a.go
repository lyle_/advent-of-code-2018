package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
)

type Claim struct {
	ID	int
}

// Read the input file and return a list of Claims
func readClaims(path string) ([]Claim, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var claims []Claim
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		//re := regexp.MustCompile(`#(\d{4}) @ 656,975: 12x17`)
		re := regexp.MustCompile(`#(\d{4}) @ (\d+),(\d+): (\d+)x(\d+)`)
		fmt.Printf("%#v\n", re.FindStringSubmatch(scanner.Text()))
		//claims = append(claims, scanner.Text())
	}
	return claims, scanner.Err()
}

func main() {
	claims, err := readClaims("input.txt")
	if err != nil {
		log.Fatalf("readClaims: %s", err)
	}
	for _, claim := range claims {
		fmt.Println("Claim ID: ", claim.ID)
	}
}
